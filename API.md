Project 5 JSON API
==================

This documents described the JSON API used between Servers and Clients. All
communication is done using WebSockets and can be used to implement a Client in
any programming language that has the ability to use WebSockets.

The Server currently uses the
[Phoenix Web Framework](http://phoenixframework.org), so much of the JSON
structure comes from that.


Connecting
----------

The Server exposes a WebSocket endpoint at:

[ws://0.0.0.0:4000/socket/websocket](ws://0.0.0.0:4000/socket/websocket)


It can also handle an HTTP-initiated UPGRADE at:

[http://0.0.0.0:4000/socket](http://0.0.0.0:4000/socket)


Joining and Staying Alive
-------------------------

Phoenix requires that Clients _join a Channel_ and send a _heartbeat_ once
every 30 seconds to stay connected to the Server.

### Join

```json
{
    "topic": "com",
    "ref": null,
    "event": "phx_join",
    "payload": {}
}
```

**Success reply:**
```json
{"topic":"com","ref":null,"payload":{"status":"ok","response":{}},
"join_ref":null,"event":"phx_reply"}
 ```

**Error reply:**
```json
{"topic":"com","ref":null,"payload":{"status":"error","response":{"reason": REASON}},
"join_ref":null,"event":"phx_reply"}
```


### Hearbeat

```json
{
    "topic": "phoenix",
    "ref": null,
    "event": "heartbeat",
    "payload": {}
}
```

**Success reply:**
```json
{"topic":"phoenix","ref":null,"payload":{"status":"ok","response":{}},
"join_ref":null,"event":"phx_reply"}
```

**Error reply:**
Clients won't receive a JSON error for a heartbeat because the Server  closes the WebSocket.


Notify
------

At any time, the Server can notify a Client of a new tweet. The JSON will be of the form:

```json
{
    "topic": "com",
    "ref": null,
    "event": "tweet",
    "payload": {
        "id": INTEGER,
        "from": USERNAME,
        "created": ISO_8601_TIMESTAMP,
        "message": TWEET_MESSAGE,
        "retweeted_status": {
            "id": ORIGINAL_TWEET_ID,
            "from": ORIGINAL_AUTHOR,
            "message": ORIGINAL_MESSAGE
        }
    }
}
```

Note that `retweeted_status` only appears for a re-tweet. Also, the properties
are not exhaustive and can contain _more_ details such as hashtags parsed and
people mentioned.


Application-level Calls
-----------------------

The remaining calls use a consistent format.

Since the communication over WebSockets is asynchronous, you can use the `ref`
property to supply a unique value which can later be parsed by the client to
match up messages with responses.

The `topic` is always `"com"`. Therefore, for the remaining examples, we only
include the properties that vary.

Also, if an error occurs at the server-level, it will be of the form:

```json
{
    ...
    "payload": {
        "status":"error",
        "response": {
            "reason": REASON
        }
    },
    "join_ref":null,
    "event":"phx_reply"
}
```

Alternatively, if the error occurs at the application-level, it will be of the form:
```json
{
    ...
    "payload": {
        "status":"ok",
        "response": {
            "error": REASON
        }
    },
    "join_ref":null,
    "event":"phx_reply"
}
```

Finally, a successfully response is of the form:
```json
{
    ...
    "payload": {
        "status":"ok",
        "response": PAYLOAD
    },
    "event":"phx_reply"
}
```

For brevity, only the Server's `PAYLOAD` is documented below:

### Register User
Client sends:
```json
{
    ...
    "event": "register_user",
    "payload": {
        "handle": USERNAME
    }
}
```

On success, the Server payload is:
```json
{}
```


### Log In
```json
{
    ...
    "event": "login",
    "payload": {
        "handle": USERNAME
    }
}
```

On success, the Server payload is:
```json
{}
```

### Log Out
```json
{
    ...
    "event": "login",
    "payload": {
        "handle": USERNAME
    }
}
```

On success, the Server payload is:
```json
{}
```


### Tweet
```json
{
    ...
    "event": "tweet",
    "payload": {
        "from": USERNAME,
        "message": MESSAGE
    }
}
```

On success, the Server payload is:
```json
{
    "id": INTEGER,
    "from": USERNAME,
    "created": ISO_8601_TIMESTAMP,
    "message": TWEET_MESSAGE
}
```

### Re-tweet
```json
{
    ...
    "event": "tweet",
    "payload": {
        "from": USERNAME,
        "original_tweet_id": TWEET_ID,
        "message": MESSAGE
    }
}
```

On success, the Server payload is:
```json
{
    "id": INTEGER,
    "from": USERNAME,
    "created": ISO_8601_TIMESTAMP,
    "message": TWEET_MESSAGE,
    "retweeted_status": {
        "id": ORIGINAL_TWEET_ID,
        "from": ORIGINAL_AUTHOR,
        "message": ORIGINAL_MESSAGE
    }
}
```


### Follow (AKA Subscribe)
```json
{
    ...
    "event": "subscribe",
    "payload": {
        "follower": USERNAME_OF_CURRENT_USER,
        "tweeter": USERNAME_OF_PERSON_TO_FOLLOW
    }
}
```

On success, the Server payload is:
```json
{}
```


### Query Tweets by Hashtag
```json
{
    ...
    "event": "query_hashtag",
    "payload": {
        "hashtag": HASHTAG
    }
}
```

On success, the Server payload is:
```json
{
    "tweets": [
        {
            "id": INTEGER,
            "from": USERNAME,
            "created": ISO_8601_TIMESTAMP,
            "message": TWEET_MESSAGE,
            "retweeted_status": {
                "id": ORIGINAL_TWEET_ID,
                "from": ORIGINAL_AUTHOR,
                "message": ORIGINAL_MESSAGE
            }
        },
        ...
    ]
}
```


### Query Tweets by People who I follow
```json
{
    ...
    "event": "query_subscriptions",
    "payload": {
        "handle": USERNAME,
        "since": ISO_8601_TIMESTAMP
    }
}
```

On success, the Server payload is:
```json
{
    "tweets": [
        {
            "id": INTEGER,
            "from": USERNAME,
            "created": ISO_8601_TIMESTAMP,
            "message": TWEET_MESSAGE,
            "retweeted_status": {
                "id": ORIGINAL_TWEET_ID,
                "from": ORIGINAL_AUTHOR,
                "message": ORIGINAL_MESSAGE
            }
        },
        ...
    ]
}
```


### Query Tweets which mention me
```json
{
    ...
    "event": "query_mentions",
    "payload": {
        "handle": USERNAME,
        "since": ISO_8601_TIMESTAMP
    }
}
```

On success, the Server payload is:
```json
{
    "tweets": [
        {
            "id": INTEGER,
            "from": USERNAME,
            "created": ISO_8601_TIMESTAMP,
            "message": TWEET_MESSAGE,
            "retweeted_status": {
                "id": ORIGINAL_TWEET_ID,
                "from": ORIGINAL_AUTHOR,
                "message": ORIGINAL_MESSAGE
            }
        },
        ...
    ]
}
```
