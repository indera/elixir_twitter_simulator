.PHONY: project5.zip clean test runner archive

build: project5
dist:  project5.zip

clean:
	rm -f project5 project5.zip

test:
	mix test --trace

project5: lib/*.ex mix.exs config/*.exs
	mix escript.build

run:
	mix phx.server


project5.zip:
	zip -r project5.zip lib/ test/ docs/ config/ out/ client/ mix.exs README API.md project4-README ws_client.js ws_client.py Makefile

archive:
	# Pack all tracked files for submission
	git archive HEAD --format=zip > submit_archive_`date +%Y_%m_%d`.zip

rsa_pub_priv:
	openssl genrsa -out rsa_2048_priv.pem 2048
	openssl rsa -in rsa_2048_priv.pem -pubout -out rsa_2048_pub.pem
