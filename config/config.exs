# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :project5, Project5Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EremVhgftbzBwDmb1zYUNZVJghPTcOUwY6OYVff970Wzd/yuTwo5dugwJswNxNmN",
  render_errors: [view: Project5Web.ErrorView, accepts: ~w(json)],
  pubsub: [name: Project5.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console, level: :info,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
