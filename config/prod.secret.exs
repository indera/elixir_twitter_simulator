use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :project5, Project5Web.Endpoint,
  secret_key_base: "S4voLjk6KTzy07ctCQF2BkZw8TlrkCAl8Aw8HAqLNl5TEDgv0DauJPiMgixBgC33"
