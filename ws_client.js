#!/usr/bin/env node

// Example client adapted from:
//      https://www.npmjs.com/package/websocket
//
//  Setup:
//      $ npm install websocket

var WebSocketClient = require('websocket').client;
var client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    connection.on('message', function(message) {
        // console.log("Received: " + JSON.stringify(message, null, 4));
        if (message.type === 'utf8') {
            console.log("Received: " + message.utf8Data);
        }
    });

    function sendNumber() {
        if (connection.connected) {
            var number = Math.round(Math.random() * 0xFFFFFF);
            connection.sendUTF(number.toString());
            setTimeout(sendNumber, 1000);
        }
    }

    function join() {
        data = {topic: "com", ref: null, event: "phx_join", payload: {}}
        connection.send(JSON.stringify(data));
    }

    function register(handle) {
        console.log("Register user: " + handle);
        data = {topic: "com", ref: null, event: "register_user", payload: {handle: handle}}
        connection.send(JSON.stringify(data));
    }

    function login(handle) {
        console.log("Login user: " + handle);
        data = {topic: "com", ref: null, event: "login", payload: {handle: handle}}
        connection.send(JSON.stringify(data));
    }

    function logout(handle) {
        console.log("Logout user: " + handle);
        data = {topic: "com", ref: null, event: "logout", payload: {handle: handle}}
        connection.send(JSON.stringify(data));
    }

    function add_random_user() {
        var handle = "user_" + Math.round(Math.random() * 0xFFFFFF);
        console.log("add_random_user: " + handle);
        register(handle);
        login(handle);
        subscribe(handle, "adobra");
        tweet("adobra", "Max #entropy reached!");
        get_counts();
        setTimeout(add_random_user, 1000);
    }

    function tweet(handle, message) {
        console.log("Tweet: " + message);
        data = {topic: "com", ref: null, event: "tweet",
            payload: {from: handle, message: message}}
        connection.send(JSON.stringify(data));
    }

    function retweet(handle, message, original_tweet_id) {
        console.log("RT: " + message + " [original_tweet_id: " + original_tweet_id + "]");
        data = {topic: "com", ref: null, event: "retweet",
            payload: {from: handle, message: message, original_tweet_id: original_tweet_id}}
        connection.send(JSON.stringify(data));
    }

    function query_hashtag(handle, hashtag) {
        console.log("Query hashtag: #" + hashtag);
        data = {topic: "com", ref: null, event: "query_hashtag",
            payload: {from: handle, hashtag: hashtag}}
        connection.send(JSON.stringify(data));
    }

    function query_subscriptions(handle, since) {
        console.log("Query subscriptions for @" + handle + " since: " + since);
        data = {topic: "com", ref: null, event: "query_subscriptions",
            payload: {handle: handle, since: since}}
        connection.send(JSON.stringify(data));
    }

    function query_mentions(handle, since) {
        console.log("Query mentions: @" + handle + " since: " + since);
        data = {topic: "com", ref: null, event: "query_mentions",
            payload: {handle: handle, since: since}}
        connection.send(JSON.stringify(data));
    }

    function subscribe(follower, tweeter) {
        console.log("Subscribe @" + follower + " to news from @" + tweeter);
        data = {topic: "com", ref: null, event: "subscribe",
            payload: {follower: follower, tweeter: tweeter}}
        connection.send(JSON.stringify(data));
    }

    function get_counts() {
        console.log("\n==> Get server counts");
        data = {topic: "com", ref: null, event: "get_counts",
            payload: {}}
        connection.send(JSON.stringify(data));
    }

    var yesterday = new Date(Date.now() - 86400000);

    join();
    register("user42");
    register("adobra");

    login("user42");
    login("adobra");

    // User42 gets a message
    subscribe("user42", "adobra")
    tweet("adobra", "Truly honest to #God...");

    tweet("user42", "Hi @adobra! Where is my #bonusANDglory?");
    retweet("user42", "Check this in/out!", 1);
    query_hashtag("user42", "God");
    query_mentions("adobra", new Date());
    query_subscriptions("user42", yesterday);

    add_random_user();

});

ws_address = "ws://0.0.0.0:4000/socket/websocket";
console.log("Connecting to: " + ws_address)
client.connect(ws_address);
