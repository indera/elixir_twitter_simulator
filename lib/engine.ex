
defmodule User do
  @moduledoc "Abstraction for a user"

  defstruct(
    handle: "",
    tweets: [],
    followers: MapSet.new, # MapSet of users which I follow
    following: MapSet.new, # MapSet of users I am interested in (@see :subscribe)
    clients: [],
    pub_key: "",    # public key received during registration
    challenge: "",  # latest challenge generated for login_secure
  )
end

defmodule Tweet do
@moduledoc "Abstraction for a message"

  defstruct(
    id: -1,
    from: "",
    created: NaiveDateTime.utc_now(),
    message: "",
    hashtags: [],
    mentions: [],

    retweeted_status: nil,  # This attribute contains a representation of the original Tweet
  )

end

defmodule Project5.Engine do
  @moduledoc """
  Server/Engine portion of a Twitter-clone.
  """
  use GenServer
  require Logger
  alias Project5.{StatsCalculator}
  import Utils
  require ExPublicKey

  @timeout 30_000

  @search_size_hashtags 50

  # GenServer Client API

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    state = %{
      users: %{},     # map of handle   => []%User
      tweets: %{},    # map of tweet_id => []%Tweet
      hashtags: %{},  # map of hash tag => []%Tweet
      mentions: %{},  # map of handle   => []%Tweet
      stats: StatsCalculator.start(self()), # Create a process to compute tweet rate
    }

    IO.puts("Stats calculator: #{inspect state.stats}")
    {:ok, state}
  end

  def get_counts(server) do
    GenServer.call(server, {:get_counts}, :infinity)
  end

  def login(server, handle) do
    GenServer.call(server, {:login, handle}, @timeout)
  end

  def login_secure(server, handle) do
    GenServer.call(server, {:login_secure, handle}, @timeout)
  end

  def login_verify(server, handle, signed_msg) do
    GenServer.call(server, {:login_verify, handle, signed_msg}, @timeout)
  end

  def logout(server, handle) do
    GenServer.call(server, {:logout, handle}, @timeout)
  end

  def register_user(server, handle) do
    GenServer.call(server, {:register_user, handle}, @timeout)
  end

  def register_user_secure(server, handle, pub_key) do
    GenServer.call(server, {:register_user_secure, handle, pub_key}, @timeout)
  end

  def tweet(server, from, message) do
    GenServer.call(server, {:tweet, from, message}, @timeout)
  end

  def subscribe(server, follower, tweeter) do
    GenServer.call(server, {:subscribe, follower, tweeter}, @timeout)
  end

  def retweet(server, from, message, original_tweet_id) do
    GenServer.call(server, {:retweet, from, message, original_tweet_id}, @timeout)
  end

  def query_hashtag(server, hashtag) do
    GenServer.call(server, {:query_hashtag, hashtag}, @timeout)
  end

  def query_subscriptions(server, handle, since) do
    GenServer.call(server, {:query_subscriptions, handle, since}, @timeout)
  end

  def query_mentions(server, handle, since) do
    GenServer.call(server, {:query_mentions, handle, since}, @timeout)
  end

  # GenServer Server API

  def handle_call({:register_user, handle}, from, state) do
    cond do
      Map.has_key?(state.users, handle) ->
        {:reply, {:error, "Username taken"}, state}

      String.match?(handle, ~r/[^a-z0-9_]/i) ->
        {:reply, {:error, "Invalid username"}, state}

      :otherwise ->
        Logger.debug(fn -> "@#{handle} registered." end)
        updated_users = Map.put_new(state.users, handle, %User{handle: handle})
        update_user_count(state, from)
        {:reply, {:ok}, %{state | users: updated_users}}
    end
  end

  def handle_call({:register_user_secure, handle, pub_key}, from, state) do
    cond do
      Map.has_key?(state.users, handle) ->
        {:reply, {:error, "Username taken"}, state}

      String.match?(handle, ~r/[^a-z0-9_]/i) ->
        {:reply, {:error, "Invalid username"}, state}

      :otherwise ->
        # TODO: verify the key?
        # System.cmd("openssl", ["rsa", "-inform", "PEM", "-pubin", "-in", "rsa_2048_pub.pem", "-noout"])
        # {"", 0}
        Logger.debug(fn -> "@#{handle} registered with pubkey: #{pub_key}" end)
        updated_users = Map.put_new(state.users, handle,
                                    %User{handle: handle, pub_key: pub_key})
        update_user_count(state, from)
        {:reply, {:ok}, %{state | users: updated_users}}
    end
  end

  @doc """
  When a message arrives at the server:
    - generate a tweet_id
    - update user's record using the tweet_id
    - notify the followers of the user
    - notify the users mentioned in the tweet
  """
  def handle_call({:tweet, from, message}, caller, state) do
    cond do
      is_nil(from) ->
        Logger.warn(fn -> "Missing `from` in call to tweet." end)
        {:reply, {:error, "Bad call"}, state}

      is_nil(state.users[from]) ->
        Logger.warn(fn -> "Unknown user: @#{from}" end)
        {:reply, {:error, "Unknown user"}, state}

      :otherwise ->
        process_tweet({from, message, nil}, caller, state)
    end
  end

  def handle_call({:retweet, from, message, original_tweet_id}, caller, state) do
    cond do
      is_nil(from) ->
        Logger.warn(fn -> "Missing `from` in call to retweet." end)
        {:reply, {:error, "Bad call"}, state}

      is_nil(original_tweet_id) ->
        Logger.warn(fn -> "Missing `original_tweet_id` in call to retweet." end)
        {:reply, {:error, "Bad call"}, state}

      is_nil(state.users[from]) ->
        Logger.warn(fn -> "Unknown user: @#{from}" end)
        {:reply, {:error, "Unknown user"}, state}

      is_nil(state.tweets[original_tweet_id]) ->
        Logger.warn(fn -> "Unknown tweet: #{original_tweet_id}" end)
        {:reply, {:error, "Unknown tweet"}, state}

      :otherwise ->
        original_tweet = state.tweets[original_tweet_id]
        process_tweet({from, message, original_tweet}, caller, state)
    end
  end

  @doc "Executed when a client `clicks` the [Follow] button"
  def handle_call({:subscribe, follower, tweeter}, from, state) when not is_list(tweeter) do
    handle_call({:subscribe, follower, [tweeter]}, from, state)
  end

  def handle_call({:subscribe, follower, tweeters}, from, state) do
    Logger.debug(fn -> "Received :subscribe to #{length(tweeters)} users from @#{follower}" end)
    pulse(state, from)
    user_follower = state.users[follower]

    all_valid =
      tweeters
      |> Enum.all?(&(Map.has_key?(state.users, &1)))

    cond do
      is_nil(user_follower) ->
        Logger.warn(fn -> "Unknown user: @#{follower}" end)
        {:reply, {:error, "Unknown user: @#{follower}"}, state}

      not all_valid ->
        Logger.warn(fn -> "Cannot follow unknown user(s)" end)
        {:reply, {:error, "Unknown tweeter(s)"}, state}

      :otherwise ->
        updated_follower = %{ user_follower |
          following: MapSet.union(user_follower.following,
                                  MapSet.new(tweeters))
        }

        updated_tweeters =
          tweeters
          |> Enum.map(fn tweeter ->
            user_tweeter = state.users[tweeter]
            {tweeter, %{ user_tweeter |
              followers: MapSet.put(user_tweeter.followers, follower)
            }}
          end)
          |> Map.new

        updated_users =
          Map.merge(state.users, updated_tweeters)
          |> Map.put(follower, updated_follower)

        new_state = %{ state | users: updated_users}
        {:reply, {:ok}, new_state}
    end
  end

  @doc """
  Return a list of last N tweets which contain the specified tag
  """
  def handle_call({:query_hashtag, hashtag}, from, state) do
    pulse(state, from)
    found =
      state.hashtags
      |> Map.get(hashtag, [])
      |> Enum.take(@search_size_hashtags)
    {:reply, {:ok, found}, state}
  end

  def handle_call({:query_subscriptions, handle, since}, from, state) do
    cond do
      is_nil(handle) ->
        {:reply, {:error, "Bad call: missing `handle`"}, state}

      is_nil(state.users[handle]) ->
        {:reply, {:error, "Unknown user: @#{handle}"}, state}

      :otherwise ->
        # Find the tweets of the users I follow
        pulse(state, from)
        found =
          state.users[handle].following
          |> Enum.flat_map(fn user -> state.users[user].tweets end)
          |> Enum.take_while(fn tweet ->
            NaiveDateTime.compare(tweet.created, since) === :gt
          end)
        {:reply, {:ok, found}, state}
    end
  end

  @doc "Find the tweets which mention a given handle"
  def handle_call({:query_mentions, handle, since}, from, state) do
    pulse(state, from)

    found =
      state.mentions
      |> Map.get(handle, [])
      |> Enum.take_while(fn tweet ->
        NaiveDateTime.compare(tweet.created, since) === :gt
      end)
    {:reply, {:ok, found}, state}
  end

  @doc "Get counts for each data structure"
  def handle_call({:get_counts}, _from, state) do
    stats = %{
      users: map_size(state.users),
      tweets: map_size(state.tweets),
      hashtags: map_size(state.hashtags),
      mentions: map_size(state.mentions),

      connected:
        state.users
        |> Map.values()
        |> Enum.map(fn user -> length(user.clients) end)
        |> Enum.sum()
    }
    {:reply, {:ok, stats}, state}
  end

  @doc "Check if a user exists and mark as `live`"
  def handle_call({:login, handle}, {client, _ref} = from, state) do
    if Map.has_key?(state.users, handle) do
      if Enum.member?(state.users[handle].clients, client) do
        Logger.debug("User @#{handle} already logged in!")
        {:reply, {:error, "Already logged in"}, state}
      else
        user = %{ state.users[handle] |
          clients: [ client | state.users[handle].clients ]
        }
        new_state = %{ state |
          users: Map.put(state.users, handle, user)
        }
        Logger.debug(fn -> "@#{handle} logged in" end)
        pulse(state, from)
        {:reply, {:ok}, new_state}
      end
    else
      Logger.warn("User @#{handle} is not registered!")
      {:reply, {:error, "User not found"}, state}
    end
  end

  @doc "Generate and store a challenge for the user to sign "
  def handle_call({:login_secure, handle}, {client, _ref} = from, state) do
    Logger.info("Handle call: login_secure")

    if Map.has_key?(state.users, handle) do

      if Enum.member?(state.users[handle].clients, client) do
        Logger.debug("User @#{handle} already logged in!")
        {:reply, {:error, "Already logged in"}, state}
      else
        challenge = get_challenge()
        Logger.info("Challenge for @#{handle} is [#{challenge}]")
        user = %{ state.users[handle] | challenge: challenge }
        new_state = %{ state |
          users: Map.put(state.users, handle, user)
        }
        pulse(state, from)
        {:reply, {:ok, challenge}, new_state}
      end
    else
      Logger.warn("User @#{handle} is not registered!")
      {:reply, {:error, "User not found"}, state}
    end
  end

  @doc "Verify the signed challenge"
  def handle_call({:login_verify, handle, signed_msg},
                  {client, _ref} = from, state) do
    if Map.has_key?(state.users, handle) do
      [tstamp_client, tstamp, msg, encoded_sig] = String.split(signed_msg, "|")
      {:ok, sig} = encoded_sig |> Base.url_decode64
      tstamp = tstamp |> String.to_integer
      tstamp_client = tstamp_client |> String.to_integer
      {:ok, dt} = tstamp |> DateTime.from_unix
      {:ok, dt_client} = tstamp_client |> DateTime.from_unix

      Logger.info("Challenge for @#{handle} is: #{msg}")
      Logger.info("Created at: #{DateTime.to_string(dt)}")
      Logger.info("Signed at: #{DateTime.to_string(dt_client)}")

      # Verify if the stamp is not too old
      if tstamp_client - tstamp > 1 do
        Logger.warn("Challenge expired!")

        # Invalidate the stored challenge
        user = %{ state.users[handle] | challenge: "" }
        new_state = %{ state |
          users: Map.put(state.users, handle, user)
        }
        {:reply, {:error, "Challenge expired."}, new_state}
      else
        Logger.info("Verify the signature for @#{handle}")
        {:ok, pub_key} = ExPublicKey.loads(state.users[handle].pub_key)

        case ExPublicKey.verify("#{tstamp_client}|#{tstamp}|#{msg}", sig, pub_key) do
          {:ok, true} ->
            user = %{ state.users[handle] |
              clients: [ client | state.users[handle].clients ]
            }
            new_state = %{ state |
              users: Map.put(state.users, handle, user)
            }
            Logger.debug(fn -> "@#{handle} logged in using RSA challenge: #{state.users[handle].challenge}" end)
            pulse(state, from)
            {:reply, {:ok}, new_state}

          :otherwise ->
            Logger.warn("Signature verification failed for: #{handle}, signature: #{sig}")
            {:reply, {:error, "A three-letter agency is trying nasty things!"}, state}
        end
      end
    else
      Logger.warn("User @#{handle} is not registered!")
      {:reply, {:error, "User not found"}, state}
    end
  end

  def handle_call({:logout, nil}, _from, state), do: {:reply, {:ok}, state}
  def handle_call({:logout, handle}, {client, _ref}, state) do
    user = state.users[handle]
    updated_user = %{ user | clients: List.delete(user.clients, client) }
    new_state = %{ state |
      users: Map.put(state.users, handle, updated_user)
    }
    Logger.debug(fn -> "@#{handle} logged out" end)
    {:reply, {:ok}, new_state}
  end

  def handle_cast({:logout, handle, client}, state) do
    user = state.users[handle]
    updated_user = %{ user | clients: List.delete(user.clients, client) }
    new_state = %{ state |
      users: Map.put(state.users, handle, updated_user)
    }
    Logger.debug(fn -> "@#{handle} logged out" end)
    {:noreply, new_state}
  end

  def handle_cast(msg, state) do
    Logger.warn("Unknown cast message: #{inspect msg}")
    {:noreply, state}
  end

  # Helper functions

  @doc "Notify the people who follow to the sender of a tweet"
  def notify_followers(state, tweet) do
    user = state.users[tweet.from]
    unless user === nil do
      notify_users(state, user.followers, tweet)
    end
  end

  def notify_mentions(state, tweet) do
    notify_users(state, tweet.mentions, tweet)
  end

  def notify_users(state, usernames, tweet) do
    recipients = if is_nil(tweet.retweeted_status) do
      usernames
    else
      # notify also the original author
      MapSet.put(MapSet.new(usernames), tweet.retweeted_status.from)
    end

    recipients
    |> Stream.map(fn username -> state.users[username] end)
    |> Stream.reject(&Kernel.is_nil/1)
    |> Stream.flat_map(fn user -> user.clients end)
    # |> Enum.each(fn client -> Client.notify(client, tweet) end)
    |> Enum.each(fn client -> notify_websocket(client, tweet) end)
  end

  def notify_websocket(client, tweet) do
    send(client, {:notify, tweet})
  end

  def process_tweet({from, message, original_tweet}, caller, state) do
    # original_tweet is non-null only for re-tweets
    tweet_id = map_size(state.tweets)
    now = NaiveDateTime.utc_now()

    if is_nil(original_tweet) do
      Logger.debug(fn -> "@#{from} tweeted: #{message}" end)
    else
      Logger.debug(fn -> "@#{from} re-tweeted: #{original_tweet.id} - #{message}" end)
    end

    tweet = %Tweet{
      id: tweet_id,
      from: from,
      message: message,
      created: now,
      hashtags: parse_hashtags(message),
      mentions: parse_mentions(message),
      retweeted_status: original_tweet
    }

    user = state.users[from]
    updated_user = %{ user | tweets: [tweet | user.tweets] }

    hashtags =
      Enum.into(tweet.hashtags, %{}, fn tag ->
        previous = Map.get(state.hashtags, tag, [])
        {tag, [ tweet | previous ]}
      end)

    mentions =
      Enum.into(tweet.mentions, %{}, fn username ->
        previous = Map.get(state.mentions, username, [])
        {username, [ tweet | previous ]}
      end)

    new_state = %{ state |
      users: Map.put(state.users, user.handle, updated_user),
      tweets: Map.put(state.tweets, tweet_id, tweet),
      hashtags: Map.merge(state.hashtags, hashtags),
      mentions: Map.merge(state.mentions, mentions),
    }

    update_tweet_count(state, caller)

    notify_followers(state, tweet)
    notify_mentions(state, tweet)

    {:reply, {:ok, tweet}, new_state}
  end

  def update_tweet_count(%{ :stats => calc }, {client, _ref}),
    do: StatsCalculator.new_tweet(calc, client)
  def update_tweet_count(_state, _from), do: :ok

  def update_user_count(%{ :stats => calc }, {client, _ref}),
    do: StatsCalculator.new_user(calc, client)
  def update_user_count(_state, _from), do: :ok

  def pulse(%{ :stats => calc }, {client, _ref}),
    do: StatsCalculator.pulse(calc, client)
  def pulse(_state, _from), do: :ok

end
