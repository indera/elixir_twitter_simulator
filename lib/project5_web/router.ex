defmodule Project5Web.Router do
  use Project5Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Project5Web do
    pipe_through :api
  end
end
