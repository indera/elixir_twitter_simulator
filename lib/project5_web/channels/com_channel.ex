defmodule Project5Web.ComChannel do

  use Phoenix.Channel, log_join: :debug, log_handle_in: false
  alias Project5.{Engine}

  @moduledoc """
  Channels

  Channels handle events from clients, so they are similar to Controllers, but
  there are two key differences. Channel events can go both directions -
  incoming and outgoing. Channel connections also persist beyond a single
  request/response cycle. Channels are the highest level abstraction for
  realtime communication components in Phoenix.

  Each Channel will implement one or more clauses of each of these four
  callback functions:
    - join/3
    - terminate/2
    - handle_in/3
    - handle_out/3.
"""

  def join("com", _message, socket) do
    # TODO: Test if this can replace the client heartbeat
    # :timer.send_interval(5_000, :ping)
    {:ok, socket}
  end

  def handle_in("register_user",
                %{ "handle" => handle },
                socket) do
    case Engine.register_user(Project5.Engine, handle) do
      {:ok} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("register_user_secure",
                %{ "handle" => handle, "pub_key" => pub_key},
                socket) do
    case Engine.register_user_secure(Project5.Engine, handle, pub_key) do
      {:ok} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("login", %{ "handle" => handle }, socket) do
    case Engine.login(Project5.Engine, handle) do
      {:ok} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("login_secure", %{ "handle" => handle }, socket) do
    case Engine.login_secure(Project5.Engine, handle) do
     {:ok, challenge} ->
        {:reply, {:ok, %{"challenge" => challenge}}, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("login_verify",
                %{ "handle" => handle, "signed_msg" => signed_msg },
                socket) do
    case Engine.login_verify(Project5.Engine, handle, signed_msg) do
      {:ok} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("logout", %{ "handle" => handle }, socket) do
    Engine.logout(Project5.Engine, handle)
    {:reply, :ok, socket}
  end

  def handle_in("tweet",
                %{ "from" => from, "message" => message},
                socket) do
    case Engine.tweet(Project5.Engine, from, message) do
      {:ok, tweet} ->
        {:reply, {:ok, tweet}, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("retweet",
                %{ "from" => from, "message" => message,
                  "original_tweet_id" => original_tweet_id},
                socket) do
    case Engine.retweet(Project5.Engine, from, message, original_tweet_id) do
      {:ok, tweet} ->
        {:reply, {:ok, tweet}, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("subscribe",
                %{ "follower" => follower, "tweeter" => tweeter },
                socket) do
    case Engine.subscribe(Project5.Engine, follower, tweeter) do
      {:ok} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:ok, %{"error" => reason}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("query_hashtag",
                %{ "hashtag" => hashtag },
                socket) do
    case Engine.query_hashtag(Project5.Engine, hashtag) do
      {:ok, tweets} ->
        {:reply, {:ok, %{"tweets" => tweets}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("query_subscriptions",
                %{ "handle" => handle, "since" => since },
                socket) do
    since = NaiveDateTime.from_iso8601!(since)
    case Engine.query_subscriptions(Project5.Engine, handle, since) do
      {:ok, tweets} ->
        {:reply, {:ok, %{"tweets" => tweets}}, socket}

      {:error, error} ->
        {:reply, {:ok, %{"error" => error}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("query_mentions",
                %{ "handle" => handle, "since" => since },
                socket) do
    since = NaiveDateTime.from_iso8601!(since)
    case Engine.query_mentions(Project5.Engine, handle, since) do
      {:ok, tweets} ->
        {:reply, {:ok, %{"tweets" => tweets}}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_in("get_counts",
                %{ },
                socket) do
    case Engine.get_counts(Project5.Engine) do
      {:ok, counts} ->
        {:reply, {:ok, counts}, socket}

      :otherwise ->
        {:reply, {:ok, %{"error" => "Bad call"}}, socket}
    end
  end

  def handle_info({:notify, tweet}, socket) do
    push socket, "tweet", tweet
    {:noreply, socket}
  end

  def handle_out(_, _, _) do
   IO.puts("handle_out")
  end
end


