defmodule Utils do

  # Note: For a glossary of terms see
  #   https://support.twitter.com/articles/166337

  require Logger

  @doc """
  Extract the valid hashtags
    From: "#a ##b1 #1c #c2 #tag! "
    Return: [a, b1, c2]
  """
  def parse_hashtags(message) do
    message
    |> (&Regex.replace(~r/\#+/, &1, "#")).()
    |> (&Regex.scan(~r/\B(\#[\pL]+)([\pL0-9]{0,})\b/u, &1)).()
    |> Enum.map(&hd/1)
    |> Enum.map(fn x -> String.replace(x, "#", "") end)
    # |> Enum.reject(&is_nil/1)
    |> Enum.uniq
  end

  def parse_mentions(message) do
    message
    |> (&Regex.replace(~r/\@+/, &1, "@")).()
    |> (&Regex.scan(~r/\B(\@[\pL0-9_]{1,15})\b/u, &1)).()
    |> Enum.map(&hd/1)
    |> Enum.map(fn x -> String.replace(x, "@", "") end)
    # |> Enum.reject(&is_nil/1)
    |> Enum.uniq
  end

  @doc "Get the number of each message in queue"
  def get_queue_stats(pid) do
    t = Task.async(fn ->
      {:messages, msg_list} = :erlang.process_info(pid, :messages)
      size = length(msg_list)

      stats =
        msg_list
        |> Enum.filter(fn ele -> match?({_, {_pid, _ref}, _call}, ele) end)
        |> Enum.map(fn {_, {_pid, _ref}, call} -> elem(call, 0) end)
        |> Enum.reduce(%{}, fn(tag, acc) -> Map.update(acc, tag, 1, &(&1 + 1)) end)

    "Messages in queue: #{size} - #{inspect stats}"
    end)
    Task.await(t)
  end

  @doc """
  Generate 32 random bytes, hash them and prefix with a timestamp.

  Note: The timestamp is stored in cleartext so we can use it
    to check if it is not too old when verifying the signed challenge.
  """
  def get_challenge(len \\ 32) do
    unix_time = DateTime.utc_now |> DateTime.to_unix
    rand_encoded = :crypto.strong_rand_bytes(len) |> Base.encode16
    rand_hashed = :crypto.hash(:sha256, rand_encoded) |> Base.encode16
    "#{unix_time}|#{rand_hashed}"
  end

end
