defmodule Project5.StatsCalculator do
  require Logger

  def start(server) do
    config = %{
      server: server,
      delay: 5,    # Print every N seconds
      cutoff: 30,  # Consider clients inactive after M seconds
    }
    stats = %{
      printed: NaiveDateTime.utc_now(),
      tweets: 0,
      users: 0,
      recent_tweets: 0,
      clients: [],
    }
    spawn_link(__MODULE__, :run, [config, stats])
  end

  def new_tweet(calc, client), do: send(calc, {:new_tweet, client, self()})
  def new_user(calc, client), do: send(calc, {:new_user, client, self()})
  def pulse(calc, client), do: send(calc, {:pulse, client, self()})

  def run(config, previous_stats) do
    now = NaiveDateTime.utc_now()
    delta = NaiveDateTime.diff(now, previous_stats.printed)
    stats =
      if delta >= config.delay do
        print_statistics(config, previous_stats, now, delta)
      else
        previous_stats
      end

    server = config.server
    timeout_ms = config.delay * 1_000

    receive do
      {:new_tweet, client, ^server} ->
        run(config, %{stats |
          tweets: stats.tweets + 1,
          recent_tweets: stats.recent_tweets + 1,
          clients: [{client, now} | stats.clients],
        })

      {:new_user, client, ^server} ->
        run(config, %{stats |
          users: stats.users + 1,
          clients: [{client, now} | stats.clients],
        })

      {:pulse, client, ^server} ->
        run(config, %{stats |
          clients: [{client, now} | stats.clients],
        })

    after
      timeout_ms ->
        run(config, stats)
    end
  end

  def print_statistics(config, stats, now, delta) do
    tweet_rate =
      case delta do
        0 -> 0.0
        _ -> stats.recent_tweets / delta
      end

    clients =
      stats.clients
      |> Enum.take_while(
        fn {_client, timestamp} ->
          NaiveDateTime.diff(now, timestamp) < config.cutoff
        end
      )

    unique =
      clients
      |> Stream.map(fn {client, _timestamp} -> client end)
      |> MapSet.new()
      |> MapSet.size()

    info =
      "#{tweet_rate} tw/sec, " <>
      "total tweets: #{stats.tweets}, " <>
      "users: #{stats.users}, " <>
      "active: #{unique}"
    Logger.info(info)
    Logger.info(fn -> Utils.get_queue_stats(config.server) end)

    %{ stats |
      printed: now,
      recent_tweets: 0,
      clients: clients,
    }
  end
end
