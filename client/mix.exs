defmodule Project5Client.Mixfile do
  use Mix.Project

  def project do
    [
      app: :project5,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps(),
      escript: [
        main_module: Project5.CLI,
        emu_args: [ "+Q 134217727" ],
      ],
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"},
      {:websockex, "~> 0.4.0"},
      {:ex_crypto, "~>0.7.1"},
    ]
  end
end
