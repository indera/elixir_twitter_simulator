defmodule Project5.Client do
  use GenServer
  require Logger
  alias Project5.Socket, as: Server
  alias Project5.{RSA}

  @timeout 180_000
  @default_ws_address "ws://0.0.0.0:4000/socket/websocket"
  @sign_delay 0  # delay for the challenge signature

  # GenServer public API

  def start_link(server \\ @default_ws_address, opts \\ []) do
    GenServer.start_link(__MODULE__,
                         [server, Keyword.get(opts, :quiet, false)],
                         timeout: @timeout)
  end

  def stop(server) do
    GenServer.stop(server)
  end

  def login(client, handle) do
    GenServer.call(client, {:login, handle}, @timeout)
  end

  def login_secure(client, handle) do
    GenServer.call(client, {:login_secure, handle}, @timeout)
  end

  def login_verify(client, handle, challenge, from) do
    # Note: `from` is provided
    GenServer.cast(client, {:login_verify, handle, challenge, from})
  end

  def login_finalize(client, handle, status, from) do
    GenServer.cast(client, {:login_finalize, handle, status, from})
  end

  def logout(client) do
    GenServer.call(client, {:logout}, @timeout)
  end

  def notify(client, tweet) do
    GenServer.cast(client, {:notify, tweet})
  end

  def register_user(pid, handle) do
    GenServer.call(pid, {:register_user, handle}, @timeout)
  end

  def register_user_secure(pid, handle) do
    GenServer.call(pid, {:register_user_secure, handle}, @timeout)
  end

  def tweet(client, message) do
    GenServer.call(client, {:tweet, message}, @timeout)
  end

  def subscribe(client, tweep) do
    GenServer.call(client, {:subscribe, tweep}, @timeout)
  end

  def retweet(client, message, tweet_id) do
    GenServer.call(client, {:retweet, message, tweet_id}, @timeout)
  end

  # == Query API
  def query_hashtag(client, hashtag) do
    GenServer.call(client, {:query_hashtag, hashtag}, @timeout)
  end

  def query_updates(client) do
    # Note: on the server we call two functions
    GenServer.call(client, {:query_updates}, @timeout)
  end

  # == Other functions

  def get_state(client) do
    GenServer.call(client, {:get_state})
  end

  def get_state_server(client) do
    GenServer.call(client, {:get_state_server})
  end

  def run(client, args)
  def run(client, nil), do: run(client, [])
  def run(_client, []), do: IO.puts(:standard_error, "What do you want to do? [register | follow | tweet | retweet | query | query-hashtag | listen ]")
  def run(client, ["register", handle]) do
    register_user(client, handle) |> IO.inspect()
  end
  def run(client, ["listen", handle]) do
    login(client, handle) |> IO.inspect()
    IO.puts("Server will push notifications. Listening...")
    receive do
      :wait_forever -> :ok
    end
  end
  def run(client, ["tweet", handle, message]) do
    login(client, handle) |> IO.inspect()
    tweet(client, message) |> IO.inspect()
  end
  def run(client, ["retweet", handle, tweet_id]),
    do: run(client, ["retweet", handle, tweet_id, ""])
  def run(client, ["retweet", handle, tweet_id, message]) do
    original_tweet_id =
      if is_number(tweet_id) do
        tweet_id
      else
        {int, ""} = Integer.parse(tweet_id)
        int
      end

    login(client, handle) |> IO.inspect()
    retweet(client, message, original_tweet_id) |> IO.inspect()
  end
  def run(client, ["follow", handle, followee]) do
    login(client, handle)
    subscribe(client, followee) |> IO.inspect()
  end
  def run(client, ["query", handle]) do
    login(client, handle) |> IO.inspect()
    query_updates(client) |> IO.inspect()
  end
  def run(client, ["query-hashtag", hashtag]) do
    query_hashtag(client, hashtag) |> IO.inspect()
  end

  # GenServer backend API

  def init([conn_info, quiet]) do
    {:ok, socket} = Server.start_link(conn_info)
    state = %{
      conn_info: conn_info,
      handle: nil,  # we store a value when the client logs in
      pub_key: "",
      priv_key: "",
      tweets: [],
      last_updated: ~N[1984-12-23 00:00:00],
      quiet: quiet,
      server: socket,
      next_ref: 0,
      pending: %{},
    }
    Server.join(socket, state.next_ref)
    on_joined = fn ("ok",%{}) ->
      spawn_link(fn -> Server.heartbeat(socket) end)
    end

    {:ok, append_pending(state, on_joined)}
  end

  def terminate(_reason, state) do
    Server.disconnect(state.server)
  end

  def handle_call({:login, handle}, from, %{ handle: nil } = state) do
    on_success = gen_server_reply(from, nil)
    new_state = %{ state | handle: handle }

    Server.login(state.server, state.next_ref, handle)
    |> async_call(on_success, new_state, state)
  end

  def handle_call({:login_secure, handle}, from, %{ handle: nil } = state) do
    myself = self()
    Logger.info("Login secure: #{handle}")

    on_success = fn
      ("ok", %{ "error" => reason }) ->
        GenServer.reply(from, {:error, reason})

      ("ok", %{ "challenge" => challenge}) ->
        login_verify(myself, handle, challenge, from)
    end

    Server.login_secure(state.server, state.next_ref, handle)
    |> async_call(on_success, state)
  end

  def handle_call({:login, _handle}, _from, state) do
    {:reply, {:ok, nil}, state}
  end

  def handle_call({:login_secure, _handle}, _from, state) do
    {:reply, {:ok, nil}, state}
  end

  def handle_call({:logout}, _from, %{ handle: nil } = state) do
    {:reply, :ok, state}
  end
  def handle_call({:logout}, from, state) do
    on_success = gen_server_reply(from, nil)
    new_state = %{ state | handle: nil }

    Server.logout(state.server, state.next_ref, state.handle)
    |> async_call(on_success, new_state)
  end

  def handle_call({:register_user, handle}, from, state) do
    on_success = gen_server_reply(from, nil)
    new_state = %{ state | handle: handle }

    Server.register_user(state.server, state.next_ref, handle)
    |> async_call(on_success, new_state)
  end

  @doc """
  Generate an RSA pub/priv key pair.
  Store the private key to sign challenge messages from the server.
  Send the public key to the server to verify the signed messages.
  """
  def handle_call({:register_user_secure, handle}, from, state) do
    on_success = gen_server_reply(from, nil)
    {priv, pub} = RSA.generate_rsa()

    {:ok, pub_key} = ExPublicKey.loads(pub)
    {:ok, priv_key} = ExPublicKey.loads(priv)

    new_state = %{ state |
      pub_key: pub_key,
      priv_key: priv_key,
    }

    Server.register_user_secure(state.server, state.next_ref, handle, pub)
    |> async_call(on_success, new_state)
  end

  def handle_call({:tweet, message}, from, state) do
    myself = self()
    on_success = fn
      ("ok", %{ "error" => reason }) ->
        GenServer.reply(from, {:error, reason})

      ("ok", tweet) ->
        GenServer.reply(from, {:ok, tweet})
        notify(myself, tweet)
    end

    Server.tweet(state.server, state.next_ref, state.handle, message)
    |> async_call(on_success, state)
  end

  def handle_call({:retweet, message, tweet_id}, from, state) do
    myself = self()
    on_success = fn
      ("ok", %{ "error" => reason }) ->
        GenServer.reply(from, {:error, reason})

      ("ok", tweet) ->
        GenServer.reply(from, {:ok, tweet})
        notify(myself, tweet)
    end

    Server.retweet(state.server, state.next_ref, state.handle, message, tweet_id)
    |> async_call(on_success, state)
  end

  def handle_call({:subscribe, tweep}, from, state) do
    Server.subscribe(state.server, state.next_ref, state.handle, tweep)
    |> async_call(gen_server_reply(from), state)
  end

  def handle_call({:query_hashtag, hashtag}, from, state) do
    Server.query_hashtag(state.server, state.next_ref, hashtag)
    |> async_call(gen_server_reply(from), state)
  end

  def handle_call({:query_updates}, from, state) do
    on_success = fn responses ->
      tweets =
        responses
        |> Map.values()
        |> Enum.flat_map(fn %{ "tweets" => tweets } -> tweets end)
      GenServer.reply(from, tweets)
    end

    pid = spawn_link(fn ->
      wait([state.next_ref, state.next_ref + 1], %{}, on_success)
    end)

    make_callback = fn ref ->
      fn
        ("ok", %{ "error" => reason }) ->
          Logger.warn(fn -> "query_updates error: #{inspect reason}" end)
          send(pid, {:response, ref, []})
        ("ok", response) ->
          send(pid, {:response, ref, response})
      end
    end

    callback1 = make_callback.(state.next_ref)
    callback2 = make_callback.(state.next_ref + 1)

    Server.query_subscriptions(state.server, state.next_ref, state.handle, state.last_updated)
    Server.query_mentions(state.server, state.next_ref + 1, state.handle, state.last_updated)

    new_state =
      state
      |> append_pending(callback1)
      |> append_pending(callback2)
    {:noreply, new_state}
  end

  def handle_call({:get_state}, _from, state) do
    {:reply, {:ok, state}, state}
  end

  # Async backend API

  def handle_cast({:notify, tweet}, state) do
    unless state.quiet do
      spawn(fn ->
        if not is_nil(Map.get(state, :handle)) do
          %{ "from" => from, "message" => message, "id" => id } = tweet
          IO.puts("[#{state.handle}]: @#{from} tweeted \"#{message}\"\t(id: #{id})")
        end
      end)
    end

    created =
      tweet
      |> Map.get("created")
      |> NaiveDateTime.from_iso8601!()

    last_updated = max(NaiveDateTime.to_erl(created),
                       NaiveDateTime.to_erl(state.last_updated))
                       |> NaiveDateTime.from_erl!
    new_state = %{ state |
      tweets: [tweet | state.tweets] |> Enum.uniq,
      last_updated: last_updated
    }
    {:noreply, new_state}
  end

  @doc """
  When server responds with a challenge we sign it and
  send it back to the server.
  """
  def handle_cast({:login_verify,
                  handle,
                  challenge, from},
                  %{ handle: nil } = state) do
    myself = self()
    Logger.info("Verify challenge: #{inspect challenge}")

    on_success = fn
      ("ok", %{ "error" => reason }) ->
        GenServer.reply(from, {:error, reason})
        {:noreply, state}

      ("ok", status ) ->
        GenServer.reply(from, {:ok, status})
        login_finalize(myself, handle, status, from)
    end

    tstamp = DateTime.utc_now |> DateTime.to_unix
    tstamp_and_msg = "#{tstamp+@sign_delay}|#{challenge}"

    Logger.info("Sign: #{tstamp_and_msg} with private key")
    # generate a SHA256 for the message and sign it with the private key
    {:ok, sig} = ExPublicKey.sign(tstamp_and_msg, state.priv_key)

    # combine the message and the signature
    signed_message = "#{tstamp_and_msg}|#{Base.url_encode64 sig}"
    Logger.debug(fn -> "signed_message: #{signed_message}" end)
    Server.login_verify(state.server, state.next_ref, handle, signed_message)
    |> async_call(on_success, state)
  end

  def handle_cast({:login_finalize, handle, status, _from}, state) do
    Logger.info("Login success - status: #{inspect status}")
    new_state = %{ state | handle: handle }
    {:noreply, new_state}
  end

  def handle_cast({:websocket_response, msg}, state) do
    case msg do
      %{"event" => "tweet", "payload" => tweet} ->
        notify(self(), tweet)
        {:noreply, state}

      %{"ref" => ref,
        "payload" => %{ "status" => status, "response" => response }
      } ->
        callback =
          state.pending
          |> Map.get(ref, fn (_do, _nothing) -> :ok end)

        callback.(status, response)
        {:noreply, %{ state | pending: Map.delete(state.pending, ref) }}

      :otherwise ->
        Logger.warn(fn -> "Received unexpected WebSocket message: #{inspect msg}" end)
        {:noreply, state}
    end
  end

  defp append_pending(state, callback) do
    %{ state |
      pending: Map.put(state.pending, state.next_ref, callback),
      next_ref: state.next_ref + 1,
    }
  end

  defp async_call(call_status, callback, state) do
    async_call(call_status, callback, state, state)
  end
  defp async_call(call_status, callback, state, old_state) do
    case call_status do
      {:error, _reason} ->
        {:reply, call_status, old_state}
      :ok ->
        {:noreply, append_pending(state, callback)}
    end
  end

  defp wait(calls, responses, callback) do
    if map_size(responses) === length(calls) do
      callback.(responses)
    else
      receive do
        {:response, ref, response} ->
          wait(calls, Map.put(responses, ref, response), callback)
      end
    end
  end

  defp gen_server_reply(from, static_response \\ :undefined) do
    fn
      ("ok", %{ "error" => reason }) ->
        GenServer.reply(from, {:error, reason})

      ("ok", response) ->
        if static_response === :undefined do
          GenServer.reply(from, {:ok, response})
        else
          GenServer.reply(from, {:ok, static_response})
        end
    end
  end
end
