defmodule Project5.CLI do
  require Logger
  alias Project5.{Client, Simulator}

  def start_server() do
    IO.puts("Sorry, to start the server, please run `mix phx.server`")
  end

  @doc "return {:ok, pid} for the client process"
  def start_client([server | args]) do
    Logger.info("Starting Twitt*r-clone client")
    {:ok, client} = Client.start_link(server)
    Client.run(client, args)
  end

  def start_simulator(server, num_clients) do
    Logger.info("Starting Twitt*r-clone simulator")
    Simulator.start(server, num_clients)
  end

  def show_usage() do
    IO.puts("""
Usage
    ./project5 -h                                   : display help
    ./project5 server                               : run as a server
    ./project5 simulator <servername> <num_clients> : run as a simulator
    ./project5 client <servername> ACTION ARGS      : run as a client

    Where ACTION is one of "register", "tweet", "query", or "query-hashtag" and
    ARGS depend on the action:
    - register HANDLE
    - listen HANDLE
    - tweet HANDLE MESSAGE
    - follow HANDLE HANDLE_OF_PERSON_TO_FOLLOW
    - query HANDLE
    - query-hashtag HASHTAG
    - retweet HANDLE TWEET_ID [MESSAGE]

    EXAMPLES

    Start the simulator with 1,000 users:
      $ ./project5 simulator ws://0.0.0.0:4000/socket/websocket 1000

    Register a user:
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket register user42

    Send a tweet:
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket tweet user42 "Hi @robot! #Elixir"

    Listen for tweets:
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket listen user42

    Follow another user:
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket follow user42 robot

    Query for recent mentions and tweets from followed users:
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket query user42

    Query for hashtag
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket query-hashtag Elixir

    Re-tweet
      $ ./project5 client ws://0.0.0.0:4000/socket/websocket retweet user42 333 "RT:"

  """)
  end

  def main(["simulator", server, num_clients]) do
    {num, ""} = Integer.parse(num_clients)
    start_simulator(server, num)
  end
  def main(["client" | args]) do
    start_client(args)
  end
  def main(["-h"]), do: show_usage()
  def main([]), do: show_usage()
  def main(["server"]), do: start_server()
end
