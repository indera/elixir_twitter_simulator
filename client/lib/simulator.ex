defmodule Project5.Simulator do
  alias Project5.{Client, SimulatedUser}
  require Logger

  # By increasing the exponent to values above 1.0 we make the Zipf curve fade
  #   quicker and decrease the amount of traffic used to notify the subscribers
  #   Note: this parameter can be changed using an environment variable
  #
  #   $ export zipf_subscribe_exponent=1.5
  @default_zipf_subscribe_exponent 1.0

  # The zipf_fidelity_factor = 0.1 corresponds to 10% of subscribers being
  #   selected according to Zipf curve and the rest is a small random number
  #   (the "tail" of the distribution)
  @zipf_fidelity_factor 0.1

  def start(server, users) do
    Logger.info("Simulator process: #{inspect self()}")
    Logger.info("Simulator: Zipf distribution for subscribers has exponent:"<>
      "#{get_zipf_subscribe_exponent()}")
    all_users = 1..users

    Logger.info("Simulator: registering #{users} users...")

    # Use one connection to register all users
    {:ok, client} = Client.start_link(server)

    all_users
    |> Stream.map(fn i ->
      Task.async(fn ->
        register(client, i)
      end)
    end)
    |> Enum.each(&Task.await/1)

    Client.stop(client)

    Logger.info("Simulator: creating simulated users using real clients...")
    Enum.each(all_users, fn i -> spawn_user(server, i) end)

    Logger.info("Simulator: subscribing users to other users...")
    stats = spawn(fn -> counter(NaiveDateTime.utc_now(), users, 0, 0) end)

    all_users
    |> Stream.map(fn i ->
      Task.async(fn ->
        client = Process.whereis(String.to_atom("User#{i}"))
        num_subscribed = subscribe(client, users, i)
        send(stats, {:update_subscribed, num_subscribed})
      end)
    end)
    |> Enum.each(fn task -> Task.await(task, :infinity) end)

    Logger.info("Simulator: setup complete.")

    receive do
      :wait_forever -> :ok
    end
  end

  def counter(_, total, users, _subscribe_count) when total === users, do: :ok
  def counter(last_printed, total_users, users_with_followers, subscribe_count) do
    delay_ms = 5_000
    now = NaiveDateTime.utc_now()
    delta = NaiveDateTime.diff(now, last_printed, :millisecond)

    if delta >= delay_ms do
      if users_with_followers === 0 do
        Logger.info("Simulator: no users have all their followers yet...")
      else
        Logger.info("Simulator: first #{users_with_followers} users have all their followers (#{subscribe_count} so far)...")
      end
      counter(now, total_users, users_with_followers, subscribe_count)
    else
      receive do
        {:update_subscribed, subscribed} ->
          counter(last_printed, total_users, users_with_followers + 1, subscribe_count + subscribed)

      after
        delay_ms ->
          counter(last_printed, total_users, users_with_followers, subscribe_count)
      end
    end
  end

  def spawn_user(server, rank) do
    if rem(rank, 100) == 0 do
      Logger.info("Start simulated user: #{rank}")
    end
    :timer.sleep(:rand.uniform(10))
    spawn(fn -> SimulatedUser.start(server, username(rank), rank) end)
  end

  def register(client, rank) do
    Logger.debug(fn -> "Registering: #{rank}" end)
    Client.register_user(client, username(rank))
  end

  def get_zipf_subscribe_exponent do
    if is_nil(System.get_env("zipf_subscribe_exponent")) do
      @default_zipf_subscribe_exponent
    else
      String.to_float(System.get_env("zipf_subscribe_exponent"))
    end
  end

  def get_subscribe_list(num_users, user_rank) when
      user_rank < num_users * @zipf_fidelity_factor do

    zipf_subscribe_exponent = get_zipf_subscribe_exponent()

    round(num_users/2)..num_users
      |> Stream.reject(fn i -> i === user_rank end)  # Don't subscribe to self.
      |> Stream.filter(fn _i ->
        :rand.uniform() <= 1/:math.pow((user_rank+1), zipf_subscribe_exponent)
      end)
      |> Stream.map(fn i -> username(i) end)
      |> Enum.to_list
  end

  def get_subscribe_list(num_users, user_rank) do
    # The majority of the users have less than 100 followers
    # https://www.statista.com/statistics/188706/follower-count-distribution-on-twitter/
    1..Enum.random(1..2)
    |> Enum.map(fn _i -> Enum.random(1..num_users) end)
    |> Enum.reject(fn i -> i === user_rank end)
    |> Enum.uniq
    |> Enum.map(fn x -> username(x) end)
  end

  def subscribe(client, num_users, user_rank) do
    list = get_subscribe_list(num_users, user_rank)

    if length(list) > 0 do
      {elapsed, _} = :timer.tc(fn ->
        Client.login(client, username(user_rank))
      end)
      Logger.debug(fn -> "Logged in @user#{user_rank} in: #{elapsed/1000} ms" end)

      {elapsed, _} = :timer.tc(fn ->
        SimulatedUser.follow(client, list)
      end)
      Logger.debug(fn -> "Subscribed #{length(list)} users to @user#{user_rank} in: #{elapsed/1000} ms" end)
    end
    length(list)
  end

  def username(number), do: "user#{number}"

  @doc """
  Conditionally follows a user based on Zipf's Law.

  - user1 should have about 1/(1+1) or 50% of users followering them
  - user2 should have 1/3, user3 1/4, and so on.
  """
  def follow_user(client, handle, rank) do
    if :rand.uniform() <= 1/(rank+1) do
      Client.login(client, handle)
      SimulatedUser.follow(client, username(rank))
      # Client.logout(client)
    end
  end
end

defmodule Project5.SimulatedUser do
  require Logger
  alias Project5.Client

  @likelihood_log_in 99
  @likelihood_log_out 1

  @likelihood_tweet_lower_bound 10
  @likelihood_tweet_upper_bound 50

  @likelihood_tweet_plain 70
  @likelihood_tweet_retweet 5
  @likelihood_tweet_reply 5
  @likelihood_tweet_mentions_and_hashtags 5
  @likelihood_query_hashtag 8
  @likelihood_query_updates 7


  def start(server, username, rank) when is_integer(rank) and rank > 0 do
    Process.flag(:trap_exit, true)

    config = %{
      server: server,
      username: username,
      rank: rank,
    }
    state = %{
      client: restart_client(server, rank),
      logged_in: false,
    }

    simulate(config, state)
  end

  defp restart_client(server, rank) do
    {:ok, client} = Client.start_link(server, quiet: true)
    # Register the process so we can re-use it for subscribing
    Process.register(client, String.to_atom("User#{rank}"))
    client
  end

  def wait, do: 901 + :rand.uniform(600) # wait between 901 and 1500 ms

  def simulate(config, state), do: simulate(config, state, wait())

  def simulate(config, state, delay) do
    receive do
      {:EXIT, _pid, _reason} ->
        Logger.info("Simulator: Client for #{config.username} shut down")
        simulate(config, %{state |
          client: restart_client(config.server, config.rank),
          logged_in: false,
        }, 10_000) # Wait 10 seconds before trying something else

    after
      delay ->
        try do
          new_state = perform_action(config, state)
          simulate(config, new_state)
        catch
          :exit, _ ->
            simulate(config, state)
        end
    end
  end

  def perform_action(config, %{:logged_in => false} = state) do
    if login?() do
      case login(state.client, config.username) do
        {:ok, nil} ->
          %{state | logged_in: true}

        :otherwise ->
          %{state | logged_in: false}
      end
    else
      state
    end
  end

  def perform_action(config, %{:logged_in => true} = state) do
    cond do
      logout?() ->
        logout(state.client)
        %{state | logged_in: false}

      send_tweet?(config.rank) ->
        tweet = pick_tweet_type()
        tweet.(state.client)
        %{state | logged_in: true}

      :otherwise ->
        state
    end
  end

  def follow(client, tweep) do
    Client.subscribe(client, tweep)
  end

  def login(client, username) do
    Client.login(client, username)
  end

  def logout(client) do
    Client.logout(client)
  end

  def reply_to_mention(client) do
    {:ok, state} = Client.get_state(client)
    tweet =
      state.tweets
      |> Enum.find(fn %{ "mentions" => mentions } -> state.handle in mentions end)

    if tweet !== nil do
      from = Map.get(tweet, "from")
      Client.tweet(client, "Thanks for the mention, @#{from}!")
    else
      Client.tweet(client, "Nobody to mention")
    end
  end

  def retweet(client) do
    {:ok, state} = Client.get_state(client)
    if length(state.tweets) > 0 do
      %{ "id" => tweet_id } =
        state.tweets
        |> Enum.at(0)
      Client.retweet(client, "RT: Tweet #{tweet_id} is interesting", tweet_id)
    end
  end

  def tweet(client) do
    Client.tweet(client, "yo tweet is BASIC!")
  end

  def tweet_with_mention_and_hashtags(client) do
    user = "user#{Enum.random(1..200)}"
    Client.tweet(client, "Check this #COP5615 @#{user} - https://twitter.com/hashtag/COP5615")
  end

  def query_hashtag(client) do
    Client.query_hashtag(client, "COP5615")
  end

  def query_updates(client) do
    Client.query_updates(client)
  end

  def login?, do: :rand.uniform(100) <= @likelihood_log_in
  def logout?, do: :rand.uniform(100) <= @likelihood_log_out

  @doc """
  Determines whether or not to send a tweet.

  The decision tries to create a Zipf distribution of tweets among all the
  users, wherein users with more followers tend to tweet more.

  `send_tweet?/1` assumes the lower the user's rank, the more followers they
  have. (See `follow_user/2`.) Normally `user1`, whose rank is 1, would
  dominate the total number of tweets and `userN`, the last user, would most
  likely not send a tweet ever.

  Lower and upper bounds have been added, however, which make the distribution
  milder. The bounds make it so `userN` will still send the occasional tweet
  and `user1` won't send as many.

  To tweak these bounds, edit the values:
    - `@likelihood_tweet_upper_bound`
    - `@likelihood_tweet_lower_bound`
  """
  def send_tweet?({_,_,rank}), do: send_tweet?(rank)
  def send_tweet?(rank) do
    :rand.uniform() <=
      (1/rank)
      |> min(@likelihood_tweet_upper_bound/100)
      |> max(@likelihood_tweet_lower_bound/100)
  end

  @doc """
  Select from possible actions with corresponding weights.

  `pick_action/2` implements Dr von Neumann's algorithm for rejection
  sampling:
   - https://en.wikipedia.org/wiki/Rejection_sampling#Algorithm

  1. let X be a a random number in [1, W], where W is sum of weights
  2. loop:
    3. let (Y, Action) be the current 2-tuple in actions
    4. if X <= Y, pick Action.
    5. otherwise, repeat.
  """
  def pick_action(actions, weights) do
    total = Enum.sum(weights)
    x = :rand.uniform(total)
    {^total, ys} =
      weights
      |> List.foldl({0, []},
        fn (weight, {sum, acc}) ->
          {weight+sum, [sum + weight | acc]}
        end)

    chosen =
      ys
      |> Enum.reverse()
      |> Enum.find_index(fn y -> x <= y end)
    Enum.at(actions, chosen)
  end

  def pick_tweet_type do
    actions = [
      &tweet/1, &retweet/1,
      &reply_to_mention/1, &tweet_with_mention_and_hashtags/1,
      &query_hashtag/1, &query_updates/1,
    ]
    weights = [
      @likelihood_tweet_plain, @likelihood_tweet_retweet,
      @likelihood_tweet_reply, @likelihood_tweet_mentions_and_hashtags,
      @likelihood_query_hashtag, @likelihood_query_updates,
    ]
    pick_action(actions, weights)
  end
end
