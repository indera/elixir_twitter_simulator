defmodule Project5.Socket do
  use WebSockex
  require Logger
  alias Poison, as: JSON

  @channel "com"
  @interval 29_000

  # https://github.com/Azolo/websockex/blob/master/lib/websockex/conn.ex#L10
  @socket_connect_timeout 200_000  # websockex default is 6_000
  @socket_recv_timeout 190_000     # websockex default is 5_000

  def start_link(conn_info, opts \\ []) do
    state = %{ owner: self() }
    opts = [
      # {:async, true},
      # {:debug, [:trace]},
      {:socket_connect_timeout, Keyword.get(
        opts, :socket_connect_timeout, @socket_connect_timeout)},
      {:socket_recv_timeout, Keyword.get(
          opts, :socket_recv_timeout, @socket_recv_timeout)},
    ]
    WebSockex.start_link(conn_info, __MODULE__, state, opts)
  end

  def disconnect(socket) do
    WebSockex.send_frame(socket, "close")
  end

  def terminate(reason, state) do
    Logger.warn("WebSockex #{inspect state} terminating with reason: #{inspect reason}")
    exit(:normal)
  end

  def join(socket, ref) do
    encode!(ref, "phx_join") |> send_message(socket)
  end

  def heartbeat(socket) do
    encode!(nil, "heartbeat", topic: "phoenix") |> send_message(socket)
    receive do
    after
      @interval ->
        heartbeat(socket)
    end
  end

  def register_user(socket, ref, handle) do
    encode!(ref, "register_user", payload: %{ handle: handle })
    |> send_message(socket)
  end

  def register_user_secure(socket, ref, handle, pub_key) do
    encode!(ref, "register_user_secure",
            payload: %{ handle: handle , pub_key: pub_key })
    |> send_message(socket)
  end

  def login(socket, ref, handle) do
    encode!(ref, "login", payload: %{ handle: handle })
    |> send_message(socket)
  end

  def login_secure(socket, ref, handle) do
    encode!(ref, "login_secure", payload: %{ handle: handle })
    |> send_message(socket)
  end

  def login_verify(socket, ref, handle, signed_msg) do
    encode!(ref, "login_verify",
            payload: %{ handle: handle, signed_msg: signed_msg })
    |> send_message(socket)
  end


  def logout(socket, ref, handle) do
    encode!(ref, "logout", payload: %{ handle: handle })
    |> send_message(socket)
  end

  def tweet(socket, ref, from, message) do
    encode!(ref, "tweet", payload: %{ from: from, message: message })
    |> send_message(socket)
  end

  def retweet(socket, ref, from, message, original_tweet_id) do
    payload = %{
      from: from,
      message: message,
      original_tweet_id: original_tweet_id,
    }
    encode!(ref, "retweet", payload: payload) |> send_message(socket)
  end

  def subscribe(socket, ref, follower, tweeter) do
    encode!(ref, "subscribe", payload: %{ follower: follower, tweeter: tweeter })
    |> send_message(socket)
  end

  def query_hashtag(socket, ref, hashtag) do
    encode!(ref, "query_hashtag", payload: %{ hashtag: hashtag })
    |> send_message(socket)
  end

  def query_subscriptions(socket, ref, handle, since) do
    encode!(ref, "query_subscriptions", payload: %{ handle: handle, since: since })
    |> send_message(socket)
  end

  def query_mentions(socket, ref, handle, since) do
    encode!(ref, "query_mentions", payload: %{ handle: handle, since: since })
    |> send_message(socket)
  end

  # WebSockex backend API

  def handle_frame({:text, "close"}, state) do
    {:close, state}
  end
  def handle_frame({:text, msg}, state) do
    Logger.debug(fn -> "WebSocket received message: #{inspect msg}" end)
    GenServer.cast(state.owner, {:websocket_response, JSON.decode!(msg)})
    {:ok, state}
  end

  # Helper functions

  def encode!(ref, event, params \\ []) do
    JSON.encode!(%{
      ref: ref,
      event: event,
      topic: Keyword.get(params, :topic, @channel),
      payload: Keyword.get(params, :payload, %{})
    })
  end

  def send_message(message, socket) do
    WebSockex.send_frame(socket, {:text, message})
  end
end
