"""
Goal: check if we can create 500 websockets sequentially

Setup:
    pip install virtualenvwrapper
    mkvirtualenv websocket-client
    pip install websocket-client

"""
import websocket
from websocket import create_connection
import json
# websocket.enableTrace(True)

WS_ADDRESS = "ws://0.0.0.0:4000/socket/websocket"

ws_list = []

for i in range(0, 500):
    print("Connecting: {}".format(i))
    ws = create_connection(WS_ADDRESS)
    ws_list.append(ws)

    # == Join
    # data = json.dumps('{"topic":"com","ref":null,"payload":{}",event":"phx_join"}')
    ws.send("{\"topic\":\"com\",\"ref\":null,\"payload\":{},\"event\":\"phx_join\"}")
    result = ws.recv()
    # print("Response: {}".format(result))

    # == Register
    ws.send("{\"topic\":\"com\",\"ref\":null,\"payload\":{\"handle\":\"user_" + "{}".format(i) + "\"},\"event\":\"register_user\"}")
    result = ws.recv()
    # print("Response: {}".format(result))

    # == Login
    ws.send("{\"topic\":\"com\",\"ref\":null,\"payload\":{\"handle\":\"user_" + "{}".format(i) + "\"},\"event\":\"login\"}")
    result = ws.recv()
    # print("Response: {}".format(result))

loop = 0

while True:
    loop = loop + 1
    print("Start loop: {}".format(loop))

    for i, ws in enumerate(ws_list):
        ws.send("{\"topic\":\"com\",\"ref\":null,\"payload\":{\"from\":\"user_" + "{}".format(i) + "\",\"message\":\"Hi!\"},\"event\":\"tweet\"}")
        result = ws.recv()
        # print("Response: {}".format(result))
        # ws.close()
